# Description

Polatouche Tout est un méta package installant Polatouche et des plugins.

Cet outil est présent pour l’instant **à des fins expérimentales uniquement**.

## Installation 

    cd ~/scripts
    git clone git@gitlab.com:magraine/polatouche/polatouche-tout.git polatouche
    cd polatouche
    composer install
    cd vendor/bin
    ln -s $(pwd)/polatouche /usr/local/bin/

### Auto-Complétion

#### Sous Linux

    ln -s $(pwd)/polatouche_console_autocomplete /etc/bash_completion.d/polatouche
    
#### Sous macOs

    ln -s $(pwd)/polatouche_console_autocomplete /usr/local/etc/bash_completion.d/polatouche

# Documentation

Se reporter au répertoire doc/ pour plus d’informations.
